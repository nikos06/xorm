module gitea.com/nikos06/xorm

go 1.11

require (
	github.com/alexbrainman/odbc v0.0.0-20200426075526-f0492dfa1575
	github.com/denisenkom/go-mssqldb v0.0.0-20200428022330-06a60b6afbbc
	github.com/go-ole/go-ole v1.2.5 // indirect
	github.com/go-sql-driver/mysql v1.5.0
	github.com/lib/pq v1.7.0
	github.com/mattn/go-sqlite3 v1.14.6
	github.com/stretchr/testify v1.4.0
	github.com/syndtr/goleveldb v1.0.0
	github.com/ziutek/mymysql v1.5.4
	modernc.org/sqlite v1.10.1-0.20210314190707-798bbeb9bb84
	xorm.io/builder v0.3.8
)
